import pika
import sys
import time

start_time = time.time()

# Generamos un mensaje de 524 bytes
mensaje = "a" * 475

# Abrimos una conexion y creamos un channel
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Desde el channel creamos una cola donde vamos a publicar los mensajes 
channel.queue_declare(queue='caso_de_uso_6')

# Recorremos 500000 veces el bucle para publicar los mensajes
for i in range(500000):
    # Publicacion de un mensaje
    channel.basic_publish(exchange='',
                        routing_key='caso_de_uso_6',
                        body=mensaje,
                        )


print("--- %s seconds ---" % (time.time() - start_time))