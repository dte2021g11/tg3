import pika
import sys
import time

start_time = time.time()

# Abrimos una conexion y creamos un channel
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_purge("caso_de_uso_2")

print("--- %s seconds ---" % (time.time() - start_time))