import pika
import sys
import time

start_time = time.time()

# Generamos un mensaje de 8388608 bytes ( 8 Mb )
mensaje = "a" * 8388559

# Abrimos una conexion y creamos un channel
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Desde el channel creamos una cola donde vamos a publicar los mensajes 
channel.queue_declare(queue='caso_de_uso_3')


# Publicacion de un mensaje de 8 Mb
channel.basic_publish(exchange='',
                    routing_key='caso_de_uso_3',
                    body=mensaje,
                    )

print("--- %s seconds ---" % (time.time() - start_time))