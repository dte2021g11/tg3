import pika
import sys
import time

start_time = time.time()

# Generamos un mensaje de 256 bytes
mensaje = "a" * 207

# Abrimos una conexion y creamos un channel
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Desde el channel creamos una cola donde vamos a publicar los mensajes 
channel.queue_declare(queue='caso_de_uso_2')

# Recorremos 100000 veces el bucle para publicar los mensajes
for i in range(100000):
    # Publicacion de un mensaje
    channel.basic_publish(exchange='',
                        routing_key='caso_de_uso_2',
                        body=mensaje,
                        )

print("--- %s seconds ---" % (time.time() - start_time))