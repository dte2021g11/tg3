from kafka import KafkaProducer
import json
import time
from kafka import KafkaConsumer
from kafka import TopicPartition

# Preparamos el tiempo actual para poder medir el tiempo de conexión y enviado
start_time = time.time()

# Creamos la conexión al message broker (puerto 9092)
producer = KafkaProducer(
    bootstrap_servers='localhost:9092',
    value_serializer=lambda v: json.dumps(v).encode('utf-8'))

# Se crea un mensaje de 524B siendo 475 repeticiones de la letra 'a'
mensaje = 'a'*475

# Enviamos 100.000 mensajes con el topic 'caso_1' y el cuerpo creado antes
for i in range(500000):
    producer.send('caso_6', mensaje)

# Creamos la conexión para el consumer
consumer = KafkaConsumer(bootstrap_servers='localhost:9092')

# Asignamos el topic a consumer
consumer.assign([TopicPartition('caso_6',2)])

# Imprimimos el tiempo de ejecución
print("--- %s seconds ---" % (time.time() - start_time))