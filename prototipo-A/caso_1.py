from kafka import KafkaProducer
import json
import time

# Preparamos el tiempo actual para poder medir el tiempo de conexión y enviado
start_time = time.time();

# Creamos la conexión al message broker (puerto 9092)
producer = KafkaProducer(
    bootstrap_servers='localhost:9092',
    value_serializer=lambda v: json.dumps(v).encode('utf-8'))

# Se crea un mensaje de 64B siendo 15 repeticiones de la letra 'a'
mensaje = 'a'*15

# Enviamos 100.000 mensajes con el topic 'caso_1' y el cuerpo creado antes
for i in range(100000):
    producer.send('caso_1', mensaje)

# Imprimimos el tiempo de ejecución
print("--- %s seconds ---" % (time.time() - start_time))