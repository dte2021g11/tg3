from kafka import KafkaProducer
import json
import time

# Preparamos el tiempo actual para poder medir el tiempo de conexión y enviado
start_time = time.time();

# Creamos la conexión al message broker (puerto 9092)
producer = KafkaProducer(
    bootstrap_servers='localhost:9092',
    value_serializer=lambda v: json.dumps(v).encode('utf-8'))

# Se crea un mensaje de 8MB siendo 8388559 repeticiones de la letra 'a'
mensaje = 'a'*8388559

# Enviamos un mensaje con el topic 'caso_1' y el cuerpo creado antes
producer.send('caso_3', mensaje)

# Imprimimos el tiempo de ejecución
print("--- %s seconds ---" % (time.time() - start_time))